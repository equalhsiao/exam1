FROM maven:3.6.1-jdk-8
COPY ./target/Exam-0.0.1.jar /app/ 
WORKDIR /app
ENTRYPOINT ["java","-jar","/app/Exam-0.0.1.jar"]
EXPOSE 8081
