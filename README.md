#概述
***
專案結合各框架的優點vert.x的非同步與spring的DI  
使用kotlin+SpringBoot+vert.x3+JPA+docker compose佈署  
將會佈署3個container(mariadb、adminer、exam)

#如何使用
***
1. git clone並切換到專案目錄下  
`cd ${your_project_parent_path}/Exam`

2. 執行init.sh將dump目錄複製到使用者目錄下(windows 使用docker toolbox執行)  
`./init.sh`

3. 執行docker compose  
`docker-compose -f stack.yml up`

4. 測試api使用[swagger inspector](https://inspector.swagger.io)、[postman](https://www.getpostman.com/downloads/)、[curl](https://curl.haxx.se/download.html)  

> * windows:  
curl -H "Content-Type: application/json" -X POST -d "{\"username\":\"skybear\"}" <font color="red">http://192.168.99.100:8081/api/user</font>
> * linux:   
curl -H "Content-Type: application/json" -X POST -d "{\"username\":\"skybear\"}" <font color="red">http://127.0.0.1:8081/api/user</font>

5. 登入adminer查看資料 伺服器:db 帳號:root 密碼:example 資料庫:exam

> * windows:
`http://192.168.99.100:8080`
> * linux:
`http://localhost:8080`
