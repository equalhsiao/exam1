CREATE DATABASE IF NOT EXISTS exam;

USE exam;

CREATE TABLE `member_master`
(
 `id`       int unsigned NOT NULL AUTO_INCREMENT ,
 `username` varchar(200) NOT NULL ,

PRIMARY KEY (`id`),
UNIQUE KEY `Ind_246` (`username`)
);

DELIMITER $$
CREATE TRIGGER member_master_trigger BEFORE INSERT ON member_master
FOR EACH ROW
BEGIN
   DECLARE next_ai INT;
   IF (EXISTS(SELECT username FROM member_master where username = NEW.username)) THEN
        SELECT auto_increment into next_ai
        FROM information_schema.tables
        WHERE table_schema=DATABASE() AND table_name = 'member_master';
        set NEW.username = CONCAT(NEW.username,next_ai);
   END IF;
END $$
DELIMITER ;








