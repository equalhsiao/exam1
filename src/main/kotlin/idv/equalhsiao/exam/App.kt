package idv.equalhsiao.exam

import idv.equalhsiao.exam.verticle.MemberMasterVerticle
import io.vertx.core.Vertx
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import javax.annotation.PostConstruct
/**
 * @author equalhsiao
 * spring啟動的主類
 **/
@Configuration
@ComponentScan(basePackages = arrayOf("idv.equalhsiao.exam"))
@EnableJpaRepositories("idv.equalhsiao.exam.dao")
@EntityScan("idv.equalhsiao.exam.po")
@SpringBootApplication(scanBasePackages = arrayOf("idv.equalhsiao.exam"))
open class App( @Autowired val memberMasterVerticle: MemberMasterVerticle ) {
	//佈署API
	@PostConstruct
	fun deployVerticle() {
		val vertx = Vertx.vertx()
		vertx.deployVerticle(memberMasterVerticle)
	}
}

fun main( args : Array<String> ) {
	runApplication<App>(*args)
}