package idv.equalhsiao.exam.configurer

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

/**
 * @author equalhsiao
 * API設定
 **/
@Component
@ConfigurationProperties( prefix = "server.api.user" )
class ServerProperties{
	//API路徑
	var path : String = "/api/user"
	//內容類型
	var contentType : String = "application/json"
	//端口
	private var port = 8081
    fun getPort(): Int {
        return port;
    }

    fun setPort( value : Int ){
        port = value
    }
}