package idv.equalhsiao.exam.dao

import idv.equalhsiao.exam.po.MemberMaster
import org.springframework.data.jpa.repository.JpaRepository

/**
 * @author equalhsiao
 * spring data實現的CRUD
 **/
interface MemberMasterJpaRepository : JpaRepository<MemberMaster,Long> , MemberMasterRepository {
}