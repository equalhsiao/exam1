package idv.equalhsiao.exam.dao

import idv.equalhsiao.exam.po.MemberMaster
/**
 * @author equalhsiao
 * 自定義會員Repository介面
 **/
interface MemberMasterRepository {
	fun saveMemberMaster( username: String ): MemberMaster
}