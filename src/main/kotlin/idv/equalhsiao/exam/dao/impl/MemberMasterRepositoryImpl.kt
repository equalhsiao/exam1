package idv.equalhsiao.exam.dao.impl

import idv.equalhsiao.exam.dao.MemberMasterRepository
import idv.equalhsiao.exam.po.MemberMaster
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
/**
 * @author equalhsiao
 * 自定義會員Repository實作
 **/
@Repository
open class MemberMasterRepositoryImpl : MemberMasterRepository {

	@PersistenceContext
	protected lateinit var entityManager: EntityManager

	//新增username
	@Transactional
	override fun saveMemberMaster( username: String ): MemberMaster {
		//沒有id所以會新增資料
		var member = entityManager.merge(MemberMaster(0, username))
		//由於重複名稱(username+id)是由mariadb的觸發器實作所以要refresh
		entityManager.refresh(member)
		return member
	}
}