package idv.equalhsiao.exam.exception

/**
 * @author equalhsiao
 * 自定義會員錯誤
 **/
open class MemberMasterException : RuntimeException {
	constructor( message: String , ex: Exception? ) : super(message, ex) {}
	constructor( message: String ) : super(message) {}
	constructor( ex: Exception ) : super(ex) {}
	constructor( exceptionEnum : MemberMasterExceptionEnum ){
		this.errorCode = exceptionEnum.errorCode
		this.errorMessage = exceptionEnum.errorMessage
	}
	
	private var errorCode : Int = 200
	private var errorMessage : String = "success" 
	
	open fun getErrorCode() : Int {
		return errorCode
	}
	open fun getErrorMessage() : String {
		return errorMessage
	}
	//會員錯誤enum
	enum class MemberMasterExceptionEnum(
	 var errorCode : Int,
	 var errorMessage : String
	) {
		_MUST_USERNAME( 400 , "username must not be null or empty" ),;
	}
}