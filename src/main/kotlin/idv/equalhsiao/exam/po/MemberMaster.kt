package idv.equalhsiao.exam.po

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Table
import javax.persistence.GenerationType
/**
 * @author equalhsiao
 **/
@Entity
@Table( name = "member_master" )
data class MemberMaster(
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY )
	@Column( name = "id" )
	val id: Long,

	@Column( name = "username", nullable = false )
	val username: String
){
	private constructor(): this(0, "")
} 