package idv.equalhsiao.exam.service

import idv.equalhsiao.exam.po.MemberMaster
import org.springframework.stereotype.Service
/**
 * @author equalhsiao
 * 會員service介面
 **/
@Service
interface MemberMasterService {
	
	fun registerUser( username : String ): MemberMaster
}