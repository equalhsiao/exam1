package idv.equalhsiao.exam.service

import idv.equalhsiao.exam.dao.MemberMasterJpaRepository
import idv.equalhsiao.exam.po.MemberMaster
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * @author equalhsiao
 * 會員相關service實作
 **/
@Service
open class MemberMasterServiceImpl(
	@Autowired val memberMasterJpaRepository: MemberMasterJpaRepository
) : MemberMasterService {
	
	override fun registerUser( username : String ): MemberMaster {
		return memberMasterJpaRepository.saveMemberMaster(username)
	}
}