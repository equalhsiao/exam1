package idv.equalhsiao.exam.verticle

import idv.equalhsiao.exam.exception.MemberMasterException
import io.vertx.core.AbstractVerticle
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.handler.BodyHandler
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.web.ServerProperties
import java.time.LocalDateTime
/**
 * @author equalhsiao
 * 共用錯誤處理 extends AbstractVerticle
 * 使用模板模式
 **/
abstract class AbstractCustomVerticle : AbstractVerticle() {

	override fun start() {
		var router = Router.router(vertx)
		//錯誤處理
		router.route().handler(BodyHandler.create()).failureHandler(
			{ failureRoutingContext ->
				//如果是自定義錯誤類型就返回memberMasterException的錯誤訊息與代碼
				if (failureRoutingContext.failure() is MemberMasterException) {
					//轉型成自定義類型
					var memberMasterException = failureRoutingContext.failure() as MemberMasterException
					failureRoutingContext.response()
						.setStatusCode(memberMasterException.getErrorCode())
						.setStatusMessage(memberMasterException.getErrorMessage())
						.end(memberMasterException.getErrorMessage())
				}
				//非自定義錯誤訊息println錯誤方便debug
				else {
					failureRoutingContext.failure().printStackTrace()
					println("uri ${failureRoutingContext.request().uri()}")
					failureRoutingContext.response()
						.setStatusCode(500)
						.setStatusMessage("please contact administer")
						.end("ERROR ${LocalDateTime.now()}")
				}
			}
		)
		//將加上錯誤處理router傳給onStart
		onStart(router)
	}

	//強制繼承的kt實作此方法
	open fun onStart( router : Router ) {}

}