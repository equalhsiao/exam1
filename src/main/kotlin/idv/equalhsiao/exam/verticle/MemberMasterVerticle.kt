@file:JvmName("MemberMasterVerticleKt")

package idv.equalhsiao.exam.verticle

import idv.equalhsiao.exam.configurer.ServerProperties
import idv.equalhsiao.exam.exception.MemberMasterException
import idv.equalhsiao.exam.exception.MemberMasterException.MemberMasterExceptionEnum
import idv.equalhsiao.exam.service.MemberMasterService
import io.vertx.core.json.Json
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.BodyHandler
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.time.LocalDateTime

/**
 * @author equalhsiao
 * 會員API
 **/
@Component
class MemberMasterVerticle(
	@Autowired val memberMasterService: MemberMasterService ,
	@Autowired val serverProperties: ServerProperties) : AbstractCustomVerticle() {
	private var log = LoggerFactory.getLogger(MemberMasterVerticle::class.java)
	//強制override抽象kt的方法
	override fun onStart(router: Router) {
		var server = vertx.createHttpServer()
		//post新增username
		router.post(serverProperties.path).consumes(serverProperties.contentType).produces(serverProperties.contentType)
			.handler({ routingContext ->
				//轉json
				var jsonObject = routingContext.getBodyAsJson()
				var response = routingContext.response()
				//如果有username就新增
				if (jsonObject.containsKey("username")) {
					var username = jsonObject.getString("username")
					//新增username
					var memberMaster = memberMasterService.registerUser(username)
					response
						.putHeader("content-type", serverProperties.contentType)
						.setStatusCode(200)
						.end(Json.encodePrettily(memberMaster))
				}
				//沒有就拋出自定義錯誤，注意:fail後面的程式仍然會執行
				else {
					routingContext.fail(MemberMasterException(MemberMasterExceptionEnum._MUST_USERNAME))
				}
			})
		//監聽path與port
		server.requestHandler(router).listen(serverProperties.getPort())
	}
}
